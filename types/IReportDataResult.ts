interface IReportDataResult {
  results: IReportData[],
  total: IReportData,
  games: IGameData[],
  platforms: IPlatformData[],
  count: number
}