import { NuxtAxiosInstance } from "@nuxtjs/axios";
import { objToQueryParams } from "~/utils/string";
const apipath = "/member_program_sale";

export interface MemberProgramSaleRepository {
  create: (payload?: any) => Promise<any>;
  list: (payload?: any) => Promise<any>;
  get: (id?: string) => Promise<any>;
  update: (payload?: any) => Promise<any>;
  delete: (id?: string) => Promise<any>;
  getReportData: (payload: {
    reportType: string;
    reportTypeBy: string;
    month?: number;
    year: number;
    customer_id?: string;
    sale_id?: string;
    member_program_id?: string;
  }) => Promise<any>;
}

export default ($axios: NuxtAxiosInstance): MemberProgramSaleRepository => ({
  async create(payload?: any): Promise<any> {
    let response = await $axios.post(apipath, payload);
    return response;
  },
  async get(id?: string): Promise<any> {
    let response = await $axios.get(`${apipath}/${id}`);
    return response;
  },
  async list(payload?: any): Promise<any> {
    let response = await $axios.get(`${apipath}/${objToQueryParams(payload)}`);
    return response;
  },
  async update(payload?: any): Promise<any> {
    let response = await $axios.put(`${apipath}/${payload?._id}`, payload);
    return response;
  },
  async delete(id?: string): Promise<any> {
    let response = await $axios.delete(`${apipath}/${id}`);
    return response;
  },
  async getReportData({
    reportType,
    reportTypeBy,
    month,
    year,
    customer_id,
    sale_id,
    member_program_id,
  }: {
    reportType: string;
    reportTypeBy: string;
    month?: number;
    year: number;
    customer_id?: string;
    sale_id?: string;
    member_program_id?: string;
  }): Promise<any> {
    let response = await $axios.get(
      `${apipath}/report/${reportTypeBy}/${reportType}/${objToQueryParams({
        month,
        year,
        customer_id,
        sale_id,
        member_program_id,
      })}`
    );
    return response;
  },
});
