interface IRequestData_Trainer_Create {
  name: string;
  price: number;
  address: string;
  contactable_info: string;
  mobileNo: string;
  lineid: string;
  facebook: string;
}
