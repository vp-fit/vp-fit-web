interface IResponseData_Report {
  results: IReportData[],
  total: IReportData,
  count: number,
  games: IResponseData_IdName[],
  platforms: IResponseData_IdName[],
}