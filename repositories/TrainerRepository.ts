import { NuxtAxiosInstance } from '@nuxtjs/axios'
import { objToQueryParams } from "~/utils/string";

export interface TrainerRepository {
  create: (payload?: any) => Promise<any>,
  get: (payload?: any) => Promise<any>,
  getDetail : (id?: string) => Promise<any>,
  update: (id: string, payload?: any) => Promise<any>
}

export default ($axios: NuxtAxiosInstance): TrainerRepository => ({
  async create(payload?: any): Promise<any> {
    let response = await $axios.post(`/trainer`, payload)
    return response
  },
  async get(payload?: any): Promise<any> {
    let response = await $axios.get(`/trainer${objToQueryParams(payload)}`)
    return response
  },
  async getDetail(id?: string): Promise<any> {
    let response = await $axios.get(`/trainer/${id}`)
    return response
  },
  async update(id: string, payload?: any): Promise<any> {
    let response = await $axios.put(`/trainer/${id}`, payload)
    return response
  },
})
