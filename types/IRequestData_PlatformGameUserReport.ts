interface IRequestData_PlatformGameUserReport {
  from: number,
  size: number,
  platformid: string,
  userid: string,
  platformuserid?: string,
  roundid?: string,
  games: any[],
  startdate: string | Date,
  enddate: string | Date,
  orderBy?: string,
  currencies?: string[],
}