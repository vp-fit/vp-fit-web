interface IPeriodDataGroup {
  name: string,
  dataGroup: IPeriodDataItem[],
}