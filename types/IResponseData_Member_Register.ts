interface IResponseData_Member_Register {
  name: string;
  address: string;
  contactable_info: string;
  mobileNo: string;
  lineid: string;
  facebook: string;
  member_type: string;
}
