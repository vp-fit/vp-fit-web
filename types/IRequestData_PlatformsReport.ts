interface IRequestData_PlatformsReport {
  startdate: string | Date,
  enddate: string | Date,
  games?: string[],
  platformids?: string[],
  gameApiId?: string,
  currencies?: string[],
}