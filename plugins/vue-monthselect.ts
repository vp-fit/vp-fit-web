import Vue from 'vue'
//@ts-ignore
import { MonthPicker } from 'vue-month-picker'
//@ts-ignore
import { MonthPickerInput } from 'vue-month-picker'

Vue.use(MonthPicker)
Vue.use(MonthPickerInput)
