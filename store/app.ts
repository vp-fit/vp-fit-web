import { Module, VuexModule, Mutation, Action } from "vuex-module-decorators";

@Module({
  name: "app",
  stateFactory: true,
  namespaced: true,
})
export default class AppModule extends VuexModule {
  isAlert: boolean = false;
  alertMessage: string = "";
  callback: Function[] = [() => {}];

  @Mutation
  setIsAlert(isAlert: boolean) {
    this.isAlert = isAlert;
  }
  @Mutation
  setCallback(callback: Function) {
    this.callback = [callback];
  }
  @Mutation
  setAlertMessage(alertMassage: string) {
    this.alertMessage = alertMassage;
  }
  @Action({ rawError: true })
  async alertCallback(callback: Function) {
    this.setCallback(callback);
  }
  @Action({ rawError: true })
  async alert(message: string) {
    this.setAlertMessage(message);
    this.setIsAlert(true);
  }
  @Action({ rawError: true })
  async closeAlert() {
    this.setAlertMessage("");
    this.setIsAlert(false);
    if (this.callback[0]) {
      this.callback[0]();
    }
    this.setCallback(()=>{});
  }

  loadingCounter: number = 0;
  @Mutation
  setLoadingCounter(loadingCounter: number) {
    this.loadingCounter = loadingCounter;
  }
  @Action({ rawError: true })
  async openLoader() {
    this.setLoadingCounter(this.loadingCounter + 1);
  }
  @Action({ rawError: true })
  async closeLoader() {
    this.setLoadingCounter(
      this.loadingCounter - 1 < 0 ? 0 : this.loadingCounter - 1
    );
  }
}
