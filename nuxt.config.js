const API_BASE_URL = process.env.API_BASE_URL;
const WEB_TITLE = process.env.WEB_TITLE;
const WEB_DESCRIPTION = process.env.WEB_DESCRIPTION;

export default {
  ssr: false,
  debug: true,
  trailingSlash: false,
  // router: {
  //   base: '/',
  // },
  head: {
    title: WEB_TITLE,
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      { name: "msapplication-TileColor", content: "#ffffff" },
      { name: "msapplication-TileImage", content: "/ms-icon-144x144.png" },
      { name: "theme-color", content: "#ffffff" },
      { hid: "title", name: "title", content: WEB_TITLE },
      { hid: "description", name: "description", content: WEB_DESCRIPTION },
      { name: "og:type", content: "website" },
      { name: "og:title", content: WEB_TITLE },
      { name: "og:description", content: WEB_DESCRIPTION },
      { name: "og:image", content: "/logo.jpg" },
      { name: "twitter:card", content: "summary_large_image" },
      // { name: 'twitter:url', content: 'https://.com/' },
      { name: "twitter:title", content: WEB_TITLE },
      { name: "twitter:description", content: WEB_DESCRIPTION },
      { name: "twitter:image", content: "/logo.jpg" },
    ],
    link: [
      { rel: "apple-touch-icon", type: "image/png", href: "/apple-icon.png" },
      {
        rel: "icon",
        type: "image/png",
        sizes: "32x32",
        href: "/favicon-32x32.png",
      },
      {
        rel: "icon",
        type: "image/png",
        sizes: "16x16",
        href: "/favicon-16x16.png",
      },
      { rel: "manifest", href: "/site.webmanifest" },
      { rel: "preconnect", href: "https://fonts.googleapis.com" },
      {
        rel: "preconnect",
        href: "https://fonts.gstatic.com",
        crossorigin: true,
      },
      { rel: "preconnect", href: "https://fonts.googleapis.com" },
      {
        rel: "stylesheet",
        href: "https://fonts.googleapis.com/css?family=Roboto:400,500,700,400italic|Material+Icons",
      },
    ],
    __dangerouslyDisableSanitizers: ["script"],
  },

  middleware: ["authentication"],
  // Global CSS (https://go.nuxtjs.dev/config-css)
  css: ["~/assets/styles/scss/main.scss"],
  styleResources: {
    scss: "~/assets/styles/scss/_variables.scss",
  },
  loading: "~/components/Loading.vue",
  // Plugins to run before rendering page (https://go.nuxtjs.dev/config-plugins)
  plugins: [
    "@/plugins/axios.ts",
    "@/plugins/repository.ts",
    { src: "~/plugins/vue-apexchart.ts", ssr: false },
    { src: "~/plugins/vue-monthselect.ts", ssr: false },
    { src: "~/plugins/vue-smooth-scrollbar.ts", ssr: false },
    // '@/plugins/apexchart.ts'
  ],
  // Auto import components (https://go.nuxtjs.dev/config-components)
  components: true,

  // Modules for dev and build (recommended) (https://go.nuxtjs.dev/config-modules)
  buildModules: ["@nuxtjs/vuetify", "@nuxt/typescript-build"],

  build: {
    loaders: {
      scss: {
        sassOptions: {
          quietDeps: true,
        },
      },
    },
  },
  vuetify: {
    theme: {
      themes: {
        dark: {
          primary: "#c39141",
          secondary: "#242424",
          gold: "#c39141",
          gray: "#B9B9B9",
          white: "#fff",
          danger: "#EB5757",
          lightblue: "#AFD9F0",
          lightgray: "#F2F2F2",
          selectblue: "#0484d2",
        },
        light: {
          primary: "#c39141",
          secondary: "#242424",
          gold: "#c39141",
          gray: "#B9B9B9",
          white: "#fff",
          danger: "#EB5757",
          lightblue: "#AFD9F0",
          lightgray: "#F2F2F2",
          selectblue: "#0484d2",
        },
      },
    },
  },

  // Modules (https://go.nuxtjs.dev/config-modules)
  modules: ["@nuxtjs/axios"],
  // Axios module configuration (https://go.nuxtjs.dev/config-axios)
  axios: {
    baseURL: API_BASE_URL,
    browserBaseURL: API_BASE_URL,
    proxy: true,
    timeout: 600000, // request timeout
  },
  auth: false,
  env: {
    API_BASE_URL: process.env.API_BASE_URL,
    WEB_TITLE: process.env.WEB_TITLE,
  },
};
