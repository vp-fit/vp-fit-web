/* eslint-disable import/no-mutable-exports */
import { NuxtAxiosInstance } from "@nuxtjs/axios";
import { Plugin } from "@nuxt/types";

const axiosPlugin: Plugin = ({ $axios }) => {
  $axios.interceptors.response.use(
    (response) => {
      return response;
    },
    (error) => {
      return Promise.reject(error);
    }
  );

  $axios.onRequest((config) => {
    // const token = localStorage.getItem("_auth.token");
    const token = `bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVmODZiOWY4ZDk1ZjAwMDA0OTAwMGE2MiIsInJvbGUiOiJhZG1pbiIsInBsYXRmb3JtSWQiOiIiLCJ1c2VybmFtZSI6ImFkbWluIiwiaWF0IjoxNjYwMDA5Nzg3LCJleHAiOjE2NjAxMDk3ODZ9.rY5MOsOCDoSzwk9OJ6hzxbRgh40B_Pqk0zw8fw4OWGg`
    if (token) {
      config.headers = {
        ...config.headers,
        Authorization: token,
      };
    }
  });

  $axios.onResponse((response) => {
    const data = response.data;
    const status = data.status || response.status;
    if (status == 200 || status == 201 || status == "ok") {
      return data.data || data;
    } else {
      throw {
        statusCode: data.statusCode,
        message: data.message,
      };
    }
  });

  $axios.onError((error: any) => {
    console.log(error, error.response)
    throw {
      statusCode: error.response.data.statusCode,
      message: error.response.data.message,
    };
  });
};

/**
 * Returns an error message from a response from web service.
 */
function getErrorMessage(response: any) {
  try {
    return `${response.data.error.code} ${response.data.error.message}`;
  } catch (e) {
    return "Something went wrong. " + response;
  }
}

export default axiosPlugin;
