import { NuxtAxiosInstance } from "@nuxtjs/axios";
import { objToQueryParams } from "~/utils/string";
const apipath = "/member_program_checkin";

export interface MemberProgramCheckinRepository {
  create: (payload?: any) => Promise<any>;
  list: (payload?: any) => Promise<any>;
  get: (id?: string) => Promise<any>;
  update: (payload?: any) => Promise<any>;
  delete: (id?: string) => Promise<any>;
  reportYear: (payload?: any) => Promise<any>;
  reportMonth: (payload?: any) => Promise<any>;
  exportReportYear: (payload?: any) => Promise<any>;
  exportReportMonth: (payload?: any) => Promise<any>;
}

export default ($axios: NuxtAxiosInstance): MemberProgramCheckinRepository => ({
  async create(payload?: any): Promise<any> {
    let response = await $axios.post(apipath, payload);
    return response;
  },
  async get(id?: string): Promise<any> {
    let response = await $axios.get(`${apipath}/${id}`);
    return response;
  },
  async list(payload?: any): Promise<any> {
    let response = await $axios.get(`${apipath}/${objToQueryParams(payload)}`);
    return response;
  },
  async update(payload?: any): Promise<any> {
    let response = await $axios.put(`${apipath}/${payload?._id}`, payload);
    return response;
  },
  async delete(id?: string): Promise<any> {
    let response = await $axios.delete(`${apipath}/${id}`);
    return response;
  },
  async reportYear(payload?: string): Promise<any> {
    let response = await $axios.get(
      `${apipath}/report/year/${objToQueryParams(payload)}`
    );
    return response;
  },
  async reportMonth(payload?: string): Promise<any> {
    let response = await $axios.get(
      `${apipath}/report/month/${objToQueryParams(payload)}`
    );
    return response;
  },
  async exportReportYear(payload?: any): Promise<any> {
    try {
      let response = await $axios.get(
        `${apipath}/report/year/export/${objToQueryParams(payload)}`,
        { responseType: "blob" }
      );

      var FILE = window.URL.createObjectURL(new Blob([response as any]));

      var docUrl = document.createElement("a");
      docUrl.href = FILE;
      docUrl.setAttribute("download", `membership_yearly_${payload.year}.xlsx`);
      document.body.appendChild(docUrl);
      docUrl.click();
    } catch (e) {
      alert("ไม่พบข้อมูลที่ค้นหา");
    }
  },
  async exportReportMonth(payload?: any): Promise<any> {
    try {
      let response = await $axios.get(
        `${apipath}/report/month/export/${objToQueryParams(payload)}`,
        { responseType: "blob" }
      );

      var FILE = window.URL.createObjectURL(new Blob([response as any]));

      var docUrl = document.createElement("a");
      docUrl.href = FILE;
      docUrl.setAttribute(
        "download",
        `membership_monthly_${payload.month}_${payload.year}.xlsx`
      );
      document.body.appendChild(docUrl);
      docUrl.click();
    } catch (e) {
      alert("ไม่พบข้อมูลที่ค้นหา");
    }
  },
});
