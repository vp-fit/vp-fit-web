interface IDashboardUser {
  info: IDashboardUserInfoData,
  username: string,
  role: string,
  platformId: string,
};