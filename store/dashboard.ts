import { NuxtAxiosInstance } from '@nuxtjs/axios';
import moment from 'moment';
import { Module, VuexModule, Mutation, Action } from 'vuex-module-decorators'
import { DashboardRepository } from '~/repositories/DashboardRepository'
import axios,{ CancelTokenSource } from 'axios'

@Module({
  name: 'dashboard',
  stateFactory: true,
  namespaced: true
})
export default class DashboardModule extends VuexModule {

  startDate: Date = new Date(moment().startOf('day').toISOString())
  endDate: Date = new Date(moment().endOf('day').toISOString())
  selectedGames: string[] = []
  selectCurrency: string = ''

  platforms: IResponseData_IdName[] = []
  games: IResponseData_IdName[] = []
  gameTypes: IGameTypeData[] = []
  gameApis: any[] = []
  currencies: IResponseData_Currencies[] = []

  todayActiveUser: IResponseData_TodayPercentStatistic | {} = {}
  todayTurnover: IResponseData_TodayPercentStatistic | {} = {}
  todayWinLose: IResponseData_TodayPercentStatistic | {} = {}

  selectPeriodTurnOver: string = "month"
  selectPeriodActiveUser: string = "month"
  selectPeriodWinLose: string = "month"
  periodTurnOver: IPeriodDataGroup[] = []
  periodActiveUser: IPeriodDataGroup[] = []
  periodWinLose: IPeriodDataGroup[] = []

  todayTopTurnoverGames: INamedData[] = []
  todayTopActiveUserGames: INamedData[] = []
  todayTopWinLoseGames: INamedData[] = []
  todayTopXRewardGames: INamedData[] = []

  platformsReport: IResponseData_PlatformsReport | {} = {}
  reportByPlatform: IResponseData_PlatformReport | {} = {}
  reportByPlatformAndGameType: IResponseData_PlatformGameTypeReport | {} = {}
  reportByPlatformGameReport: IResponseData_PlatformGameReport | {} = {}
  reportByPlatformGameUserReport: IResponseData_PlatformGameReport | {} = {}

  get platformsById() {
    const platforms = this.platforms
    const platformsById: { [key: string]: IResponseData_IdName } = {}
    for (let i = 0; i < platforms.length; i++) {
      const platform = platforms[i]
      platformsById[platform.id] = platform
    }
    return platformsById
  }

  @Mutation
  setStartDateEndDate(data: Date[]) {
    this.startDate = data[0]
    this.endDate = data[1]
  }

  @Mutation
  setSelectedGame(selectedGames: string[]) {
    this.selectedGames = selectedGames
  }

  setSelectedCurrency(selectCurrency: string) {
    this.selectCurrency = selectCurrency
  }

  @Mutation
  setPlatforms(data: IPlatformData[]) {
    this.platforms = data
  }
  
  @Mutation
  setCurrencies(data: ICurrencyData[]) {
    this.currencies = data
  }

  @Mutation
  setGames(data: IResponseData_IdName[]) {
    this.games = data
  }

  @Mutation
  setGameTypes(data: IGameTypeData[]) {
    this.gameTypes = data
  }

  @Mutation
  setGameApis(data: any[]) {
    this.gameApis = data
  }

  @Mutation
  setTodayTurnover(data: IResponseData_TodayPercentStatistic | {}) {
    this.todayTurnover = data
  }

  @Mutation
  setTodayActiveUser(data: IResponseData_TodayPercentStatistic | {}) {
    this.todayActiveUser = data
  }

  @Mutation
  setTodayWinLose(data: IResponseData_TodayPercentStatistic | {}) {
    this.todayWinLose = data
  }

  @Mutation
  setPeriodTurnover(data: IPeriodDataGroup[]) {
    this.periodTurnOver = data
  }

  @Mutation
  setPeriodActiveUser(data: IPeriodDataGroup[]) {
    this.periodActiveUser = data
  }

  @Mutation
  setPeriodWinLose(data: IPeriodDataGroup[]) {
    this.periodWinLose = data
  }

  @Mutation
  setTodayTopTurnoverGames(data: INamedData[]) {
    this.todayTopTurnoverGames = data
  }

  @Mutation
  setTodayTopActiveUserGames(data: INamedData[]) {
    this.todayTopActiveUserGames = data
  }

  @Mutation
  setTodayTopWinLoseGames(data: INamedData[]) {
    this.todayTopWinLoseGames = data
  }

  @Mutation
  setTodayTopXRewardGames(data: INamedData[]) {
    this.todayTopXRewardGames = data
  }

  @Mutation
  setPlatformsReport(data: IResponseData_PlatformsReport | {}) {
    this.platformsReport = data
  }

  @Mutation
  setReportByPlatform(data: IResponseData_PlatformsReport | {}) {
    this.reportByPlatform = data
  }

  @Mutation
  setReportByPlatformAndGameType(data: IResponseData_PlatformsReport | {}) {
    this.reportByPlatformAndGameType = data
  }

  @Mutation
  setReportByPlatformGameReport(data: IResponseData_PlatformGameReport | {}) {
    this.reportByPlatformGameReport = data
  }

  @Mutation
  setReportByPlatformGameUserReport(data: IResponseData_PlatformGameUserReport | {}) {
    this.reportByPlatformGameUserReport = data
  }

  @Mutation
  setSelectPeriodActiveUser(data: string) {
    this.selectPeriodActiveUser = data
  }

  @Mutation
  setSelectPeriodTurnOver(data: string) {
    this.selectPeriodTurnOver = data
  }

  @Mutation
  setSelectPeriodWinLose(data: string) {
    this.selectPeriodWinLose = data
  }

  @Action({ rawError: true })
  async loadPlatforms() {
    //@ts-ignore
    const dashboardRepository: DashboardRepository = $nuxt.$repositories.dashboard

    const result = await dashboardRepository.loadPlatforms()
    this.setPlatforms(result)
  }

  @Action({ rawError: true })
  async loadCurrencies() {
    //@ts-ignore
    const dashboardRepository: DashboardRepository = $nuxt.$repositories.dashboard

    const result = await dashboardRepository.loadCurrencies()
    this.setCurrencies(result)
  }

  @Action({ rawError: true })
  async loadGames() {
    //@ts-ignore
    const dashboardRepository: DashboardRepository = $nuxt.$repositories.dashboard

    let result = await dashboardRepository.loadGames()
    if (result) {
      result = result.sort((a, b) => (a.name > b.name) ? 1 : ((b.name > a.name) ? -1 : 0))
    }
    this.setGames(result)
  }

  @Action({ rawError: true })
  async loadGameTypes() {
    //@ts-ignore
    const dashboardRepository: DashboardRepository = $nuxt.$repositories.dashboard

    const result = await dashboardRepository.loadGameTypes()
    this.setGameTypes(result)
  }

  @Action({ rawError: true })
  async loadGameApis() {
    //@ts-ignore
    const dashboardRepository: DashboardRepository = $nuxt.$repositories.dashboard

    const result = await dashboardRepository.loadGameApis()
    this.setGameTypes(result)
  }

  loadOverviewCancelSource?: CancelTokenSource
  @Action({ rawError: true })
  async loadTodayActiveUser(platformid: string = '') {
    //@ts-ignore
    const dashboardRepository: DashboardRepository = $nuxt.$repositories.dashboard
    this.setTodayActiveUser({})
    const result = await dashboardRepository.loadTodayActiveUser(platformid,this.loadOverviewCancelSource?.token)
    this.setTodayActiveUser(result)
  }

  @Action({ rawError: true })
  async loadTodayTurnover(platformid: string = '') {
    //@ts-ignore
    const dashboardRepository: DashboardRepository = $nuxt.$repositories.dashboard
    this.setTodayTurnover({})
    const result = await dashboardRepository.loadTodayTurnover(platformid,this.loadOverviewCancelSource?.token)
    this.setTodayTurnover(result)
  }

  @Action({ rawError: true })
  async loadTodayWinLose(platformid: string = '') {
    //@ts-ignore
    const dashboardRepository: DashboardRepository = $nuxt.$repositories.dashboard
    this.setTodayWinLose({})
    const result = await dashboardRepository.loadTodayWinLose(platformid,this.loadOverviewCancelSource?.token)
    this.setTodayWinLose(result)
  }

  @Action({ rawError: true })
  async loadTodayTopActiveUserGames(platformid: string = '') {
    //@ts-ignore
    const dashboardRepository: DashboardRepository = $nuxt.$repositories.dashboard
    this.setTodayTopActiveUserGames([])
    const result = await dashboardRepository.loadTodayTopActiveUserGames(platformid,this.loadOverviewCancelSource?.token)
    this.setTodayTopActiveUserGames(result)
  }

  @Action({ rawError: true })
  async loadTodayTopTurnoverGames(platformid: string = '') {
    //@ts-ignore
    const dashboardRepository: DashboardRepository = $nuxt.$repositories.dashboard
    this.setTodayTopTurnoverGames([])
    const result = await dashboardRepository.loadTodayTopTurnoverGames(platformid,this.loadOverviewCancelSource?.token)
    this.setTodayTopTurnoverGames(result)
  }

  @Action({ rawError: true })
  async loadTodayTopWinLoseGames(platformid: string = '') {
    //@ts-ignore
    const dashboardRepository: DashboardRepository = $nuxt.$repositories.dashboard
    this.setTodayTopWinLoseGames([])
    const result = await dashboardRepository.loadTodayTopWinLoseGames(platformid,this.loadOverviewCancelSource?.token)
    this.setTodayTopWinLoseGames(result)
  }

  @Action({ rawError: true })
  async loadTodayTopXRewardGames(platformid: string = '') {
    //@ts-ignore
    const dashboardRepository: DashboardRepository = $nuxt.$repositories.dashboard
    this.setTodayTopXRewardGames([])
    const result = await dashboardRepository.loadTodayTopXRewardGames(platformid,this.loadOverviewCancelSource?.token)
    this.setTodayTopXRewardGames(result)
  }

  @Action({ rawError: true })
  async loadPeriodTurnover(platformid: string = '') {
    //@ts-ignore
    const dashboardRepository: DashboardRepository = $nuxt.$repositories.dashboard
    this.setPeriodTurnover([])
    const result = await dashboardRepository.loadPeriodTurnover(this.selectPeriodTurnOver, platformid,this.loadOverviewCancelSource?.token)
    this.setPeriodTurnover(result)
  }

  @Action({ rawError: true })
  async loadPeriodActiveUser(platformid: string = '') {
    //@ts-ignore
    const dashboardRepository: DashboardRepository = $nuxt.$repositories.dashboard
    this.setPeriodActiveUser([])
    const result = await dashboardRepository.loadPeriodActiveUser(this.selectPeriodActiveUser, platformid,this.loadOverviewCancelSource?.token)
    this.setPeriodActiveUser(result)
  }

  @Action({ rawError: true })
  async loadPeriodWinLose(platformid: string = '') {
    //@ts-ignore
    const dashboardRepository: DashboardRepository = $nuxt.$repositories.dashboard
    this.setPeriodWinLose([])
    const result = await dashboardRepository.loadPeriodWinLose(this.selectPeriodWinLose, platformid,this.loadOverviewCancelSource?.token)
    this.setPeriodWinLose(result)
  }

  @Action({ rawError: true })
  async reCancelTokenLoadOverview() {
    this.loadOverviewCancelSource = axios.CancelToken.source()
  }

  @Action({ rawError: true })
  async cancelLoadOverview() {
    if(this.loadOverviewCancelSource) {
      this.loadOverviewCancelSource.cancel()
    }
  }

  loadReportCancelSource?: CancelTokenSource
  @Action({ rawError: true })
  async loadPlatformsReport(request: IRequestData_PlatformsReport) {
    //@ts-ignore
    const dashboardRepository: DashboardRepository = $nuxt.$repositories.dashboard
    this.setPlatformsReport({})
    if(this.loadReportCancelSource) {
      this.loadReportCancelSource.cancel()
    }
    this.loadReportCancelSource = axios.CancelToken.source()
    const result = await dashboardRepository.loadPlatformsReport(request,this.loadReportCancelSource.token)
    this.setPlatformsReport(result)
  }

  @Action({ rawError: true })
  async loadReportByPlatform(request: IRequestData_PlatformReport) {
    //@ts-ignore
    const dashboardRepository: DashboardRepository = $nuxt.$repositories.dashboard
    this.setReportByPlatform({})
    if(this.loadReportCancelSource) {
      this.loadReportCancelSource.cancel()
    }
    this.loadReportCancelSource = axios.CancelToken.source()
    const result = await dashboardRepository.loadReportByPlatform(request,this.loadReportCancelSource.token)
    this.setReportByPlatform(result)
  }

  @Action({ rawError: true })
  async loadReportByPlatformAndGameType(request: IRequestData_PlatformGameTypeReport) {
    //@ts-ignore
    const dashboardRepository: DashboardRepository = $nuxt.$repositories.dashboard
    this.setReportByPlatformAndGameType({})
    if(this.loadReportCancelSource) {
      this.loadReportCancelSource.cancel()
    }
    this.loadReportCancelSource = axios.CancelToken.source()
    const result = await dashboardRepository.loadReportByPlatformAndGameType(request, this.loadReportCancelSource.token)
    this.setReportByPlatformAndGameType(result)
  }

  @Action({ rawError: true })
  async loadUserListReportByPlatformAndGame(request: IRequestData_PlatformGameReport) {
    //@ts-ignore
    const dashboardRepository: DashboardRepository = $nuxt.$repositories.dashboard
    this.setReportByPlatformGameReport({})
    if(this.loadReportCancelSource) {
      this.loadReportCancelSource.cancel()
    }
    this.loadReportCancelSource = axios.CancelToken.source()
    const result = await dashboardRepository.loadReportByPlatformAndGame(request, this.loadReportCancelSource.token)
    this.setReportByPlatformGameReport(result)
  }

  @Action({ rawError: true })
  async loadReportByPlatformAndGameAndUser(request: IRequestData_PlatformGameUserReport) {
    //@ts-ignore
    const dashboardRepository: DashboardRepository = $nuxt.$repositories.dashboard
    this.setReportByPlatformGameUserReport({})
    if(this.loadReportCancelSource) {
      this.loadReportCancelSource.cancel()
    }
    this.loadReportCancelSource = axios.CancelToken.source()
    const result: any = await dashboardRepository.loadReportByPlatformAndGameAndUser(request, this.loadReportCancelSource.token)
    // if(result && result.results && result.results.length) {
    //   for (var i = 0 ; i < result.results.length ; i++) {
    //     try {
    //       const urlResult = await dashboardRepository.loadGameResultUrl(result.results[i].round_id, result.results[i].id)
    //       result.results[i].game_result_url = urlResult
    //     } catch (error) {}
    //   }
    // }
    this.setReportByPlatformGameUserReport(result)
  }

  @Action({ rawError: true })
  async cancelLoadReport() {
    if(this.loadReportCancelSource) {
      this.loadReportCancelSource.cancel()
    }
  }
}
