interface IGameData{
  id?: string;
  _id: string,
  name: string,
  display_name: string,
  type: string,
  pattern: string,
  url: string,
  backend_url: string,
  status: string,
};