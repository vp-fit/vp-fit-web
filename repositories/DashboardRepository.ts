import { NuxtAxiosInstance } from '@nuxtjs/axios'
import { CancelToken } from 'axios'

export interface DashboardRepository {
  loadPlatforms: (cancelToken?: CancelToken) => Promise<IPlatformData[]>
  loadCurrencies: (cancelToken?: CancelToken) => Promise<ICurrencyData[]>
  loadGames: (cancelToken?: CancelToken) => Promise<IResponseData_IdName[]>
  loadGameTypes: (cancelToken?: CancelToken) => Promise<IGameTypeData[]>
  loadGameApis: (cancelToken?: CancelToken) => Promise<any[]>
  loadPlatformsReport: (request: IRequestData_PlatformsReport, cancelToken?: CancelToken) => Promise<IResponseData_PlatformsReport>
  loadReportByPlatform: (request: IRequestData_PlatformReport, cancelToken?: CancelToken) => Promise<IResponseData_PlatformReport>
  loadReportByPlatformAndGameType: (request: IRequestData_PlatformGameTypeReport, cancelToken?: CancelToken) => Promise<IResponseData_PlatformGameTypeReport>
  loadReportByPlatformAndGame: (request: IRequestData_PlatformGameReport, cancelToken?: CancelToken) => Promise<IResponseData_PlatformGameReport>
  loadReportByPlatformAndGameAndUser: (request: IRequestData_PlatformGameUserReport, cancelToken?: CancelToken) => Promise<IResponseData_PlatformGameUserReport>
  loadTodayActiveUser: (platformid: string, cancelToken?: CancelToken) => Promise<IResponseData_TodayPercentStatistic>
  loadTodayTurnover: (platformid: string, cancelToken?: CancelToken) => Promise<IResponseData_TodayPercentStatistic>
  loadTodayWinLose: (platformid: string, cancelToken?: CancelToken) => Promise<IResponseData_TodayPercentStatistic>
  loadTodayTopActiveUserGames: (platformid: string, cancelToken?: CancelToken) => Promise<INamedData[]>
  loadTodayTopTurnoverGames: (platformid: string, cancelToken?: CancelToken) => Promise<INamedData[]>
  loadTodayTopWinLoseGames: (platformid: string, cancelToken?: CancelToken) => Promise<INamedData[]>
  loadTodayTopXRewardGames: (platformid: string, cancelToken?: CancelToken) => Promise<INamedData[]>
  loadPeriodActiveUser: (period: string, platformid: string, cancelToken?: CancelToken) => Promise<IPeriodDataGroup[]>
  loadPeriodTurnover: (period: string, platformid: string, cancelToken?: CancelToken) => Promise<IPeriodDataGroup[]>
  loadPeriodWinLose: (period: string, platformid: string, cancelToken?: CancelToken) => Promise<IPeriodDataGroup[]>
  loadGameResultUrl: (roundid: string, txnid: string) => Promise<{ url: string }>
}

export default ($axios: NuxtAxiosInstance): DashboardRepository => ({
  async loadPlatforms(cancelToken?: CancelToken): Promise<IPlatformData[]> {
    let response: IPlatformData[] = await $axios.get(`/dashboard/platforms`, { cancelToken })
    return response
  },
  async loadGames(cancelToken?: CancelToken): Promise<IResponseData_IdName[]> {
    let response: IResponseData_IdName[] = await $axios.get(`/dashboard/games`, { cancelToken })
    return response
  },
  async loadGameTypes(cancelToken?: CancelToken): Promise<IGameTypeData[]> {
    let response: IGameTypeData[] = await $axios.get(`/dashboard/gameTypes`, { cancelToken })
    return response
  },
  async loadGameApis(cancelToken?: CancelToken): Promise<any[]> {
    let response: IGameTypeData[] = await $axios.get(`/dashboard/gameApis`, { cancelToken })
    return response
  },
  async loadPlatformsReport(request: IRequestData_PlatformsReport, cancelToken?: CancelToken): Promise<IResponseData_PlatformsReport> {
    let response: IResponseData_PlatformsReport = await $axios.post(`/dashboard/report/platforms`, request, { cancelToken })
    return response
  },
  async loadReportByPlatform(request: IRequestData_PlatformReport, cancelToken?: CancelToken): Promise<IResponseData_PlatformReport> {
    let response: IResponseData_PlatformReport = await $axios.post(`/dashboard/report/byPlatform`, request, { cancelToken })
    return response
  },
  async loadReportByPlatformAndGameType(request: IRequestData_PlatformGameTypeReport, cancelToken?: CancelToken): Promise<IResponseData_PlatformGameTypeReport> {
    let response: IResponseData_PlatformsReport = await $axios.post(`/dashboard/report/byPlatformAndGameType`, request, { cancelToken })
    return response
  },
  async loadReportByPlatformAndGame(request: IRequestData_PlatformGameReport, cancelToken?: CancelToken): Promise<IResponseData_PlatformGameReport> {
    let response: IResponseData_PlatformGameReport = await $axios.post(`/dashboard/report/byPlatformAndGame`, request, { cancelToken })
    return response
  },
  async loadReportByPlatformAndGameAndUser(request: IRequestData_PlatformGameUserReport, cancelToken?: CancelToken): Promise<IResponseData_PlatformGameUserReport> {
    let response: IResponseData_PlatformGameReport = await $axios.post(`/dashboard/report/byPlatformAndGameAndUser`, request, { cancelToken })
    return response
  },
  async loadTodayActiveUser(platformid: string = '', cancelToken?: CancelToken): Promise<IResponseData_TodayPercentStatistic> {
    let response: IResponseData_TodayPercentStatistic = await $axios.post(`/dashboard/overview/todayActiveUser`, {
      platformid
    }, { cancelToken })
    return response
  },
  async loadTodayTurnover(platformid: string = '', cancelToken?: CancelToken): Promise<IResponseData_TodayPercentStatistic> {
    let response: IResponseData_TodayPercentStatistic = await $axios.post(`/dashboard/overview/todayTurnover`, {
      platformid
    }, { cancelToken })
    return response
  },
  async loadTodayWinLose(platformid: string = '', cancelToken?: CancelToken): Promise<IResponseData_TodayPercentStatistic> {
    let response: IResponseData_TodayPercentStatistic = await $axios.post(`/dashboard/overview/todayWinLose`, { platformid }, { cancelToken })
    return response
  },
  async loadTodayTopActiveUserGames(platformid: string = '', cancelToken?: CancelToken): Promise<INamedData[]> {
    let response: INamedData[] = await $axios.post(`/dashboard/overview/todayTopActiveUser`, { platformid }, { cancelToken })
    return response
  },
  async loadTodayTopTurnoverGames(platformid: string = '', cancelToken?: CancelToken): Promise<INamedData[]> {
    let response: INamedData[] = await $axios.post(`/dashboard/overview/todayTopTurnover`, {
      platformid
    }, { cancelToken })
    return response
  },
  async loadTodayTopWinLoseGames(platformid: string = '', cancelToken?: CancelToken): Promise<INamedData[]> {
    let response: INamedData[] = await $axios.post(`/dashboard/overview/todayTopWinLose`, {
      platformid
    }, { cancelToken })
    return response
  },
  async loadTodayTopXRewardGames(platformid: string = '', cancelToken?: CancelToken): Promise<INamedData[]> {
    let response: INamedData[] = await $axios.post(`/dashboard/overview/todayTopXReward`, {
      platformid
    }, { cancelToken })
    return response
  },
  async loadPeriodActiveUser(period: string, platformid: string = '', cancelToken?: CancelToken): Promise<IPeriodDataGroup[]> {
    let response: IPeriodDataGroup[] = await $axios.post(`/dashboard/overview/periodActiveUser`, { period, platformid }, { cancelToken })
    return response
  },
  async loadPeriodTurnover(period: string, platformid: string = '', cancelToken?: CancelToken): Promise<IPeriodDataGroup[]> {
    let response: IPeriodDataGroup[] = await $axios.post(`/dashboard/overview/periodTurnover`, { period, platformid }, { cancelToken })
    return response
  },
  async loadPeriodWinLose(period: string, platformid: string = '', cancelToken?: CancelToken): Promise<IPeriodDataGroup[]> {
    let response: IPeriodDataGroup[] = await $axios.post(`/dashboard/overview/periodWinLose`, { period, platformid }, { cancelToken })
    return response
  },
  async loadGameResultUrl(roundid: string, txnid: string): Promise<{ url: string }> {
    let response: { url: string } = await $axios.post(`/dashboard/gameResultUrl`, { roundid, txnid })
    return response
  },
  async loadCurrencies(cancelToken?: CancelToken): Promise<ICurrencyData[]> {
    let response: ICurrencyData[] = await $axios.get(`/currencies/`, { cancelToken })
    return response
  }
})
