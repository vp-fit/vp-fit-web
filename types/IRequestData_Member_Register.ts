interface IRequestData_Member_Register {
  name: string;
  address: string;
  contactable_info: string;
  contactable_info_number: string;
  mobileNo: string;
  lineid: string;
  facebook: string;
  member_type: string;
  gender: string;
  note: string;
  start_date: string;
  expiry_date: string;
  // current_program: string;
  // trainer: any
}
