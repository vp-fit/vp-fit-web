/* eslint-disable import/no-mutable-exports */
import { NuxtAxiosInstance } from '@nuxtjs/axios'
import {Plugin} from '@nuxt/types'
import createRepository from '~/repositories';
import { Inject } from '@nuxt/types/app';

const repositoryPlugin: Plugin =({$axios}, inject: Inject) => {
    inject('repositories', createRepository($axios));
}

export default repositoryPlugin