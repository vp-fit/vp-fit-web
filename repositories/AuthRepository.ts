import { NuxtAxiosInstance } from '@nuxtjs/axios'

export interface AuthRepository {
  login: (request: IRequestData_Login) => Promise<IResponseData_Login>
  // profile: () => Promise<IUserResponse>
}

export default ($axios: NuxtAxiosInstance): AuthRepository => ({
  async login(request: IRequestData_Login): Promise<IResponseData_Login> {
    let response = (await $axios.post(`/user/login`, request)) as any
    if(response.error) {
      const errorMsg = response.error[0]
      throw new Error(errorMsg)
    }
    return response
  },
  // profile(): Promise<IUserResponse> {
  //   return $axios.get(`/me`);
  // },
})