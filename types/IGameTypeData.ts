interface IGameTypeData {
  type: string,
  games: IGameData[],
}