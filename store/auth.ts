import { Module, VuexModule, Mutation, Action } from 'vuex-module-decorators'
import { AuthRepository } from '~/repositories/AuthRepository'

var jwtPayloadDecoder = require('jwt-payload-decoder')


@Module({
  name: 'auth',
  stateFactory: true,
  namespaced: true
})
export default class AuthModule extends VuexModule {

  token = ''
  // userData: IUserResponse | null = null

  get isLoggedIn(): boolean {
    return !!this.token
  }

  get payload(): any {

    if (this.isLoggedIn) {
      var payload = jwtPayloadDecoder.getPayload(this.token)

      return payload
    }
    return false
  }

  get role(): string {
    if (this.payload) {
      return this.payload.role
    }
    return ""
  }

  get isGameApiRole(): boolean {
    if (this.payload) {
      return this.payload.role === "game_api"
    }
    return false
  }

  get isGameAdminRole(): boolean {
    if (this.payload) {
      return this.payload.role === "admin"
    }
    return false
  }

  get isPlatformRole(): boolean {
    if (this.payload) {
      return this.payload.role === "platform"
    }
    return false
  }

  get gameApiId(): string {
    // return true;
    if (this.payload) {
      return this.payload.gameApiId
    }
    return ""
  }

  @Mutation
  setToken(token: string) {
    this.token = token
  }
  // @Mutation
  // setUserData(userData: IUserResponse) {
  //   this.userData = userData
  // }

  // @Action({ rawError: true })
  // async fetchUser() {
  //   const token = localStorage.getItem('_auth.token') || this.token
  //   this.setToken(token)
  //   //@ts-ignore
  //   const authRepository: AuthRepository = $nuxt.$repositories.auth
  //   try {
  //     const response = await authRepository.profile()
  //     console.log(response)
  //     // this.setUserData(response)
  //   } catch (e) {
  //     this.setToken('')
  //     localStorage.removeItem('_auth.token')
  //     if (process.client) {
  //       location.reload();
  //     }
  //   }
  // }
  @Action({ rawError: true })
  async login(request: IRequestData_Login, isRemember: boolean = true) {
    //@ts-ignore
    // const authRepository: AuthRepository = $nuxt.$repositories.auth
    // const response = await authRepository.login(request)
    // const token = `bearer ${response.token}`
    // this.setToken(token)
    // localStorage.setItem('_auth.token', token)
    console.log('request', request.username, request.username == 'admin', request.password, request.password == 'p@55w0rd')
    if (request.username == 'admin' && request.password == 'P@55w0rd') {
      const token = `bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVmODZiOWY4ZDk1ZjAwMDA0OTAwMGE2MiIsInJvbGUiOiJhZG1pbiIsInBsYXRmb3JtSWQiOiIiLCJ1c2VybmFtZSI6ImFkbWluIiwiaWF0IjoxNjYwMDA5Nzg3LCJleHAiOjE2NjAxMDk3ODZ9.rY5MOsOCDoSzwk9OJ6hzxbRgh40B_Pqk0zw8fw4OWGg`
      this.setToken(token)
      // localStorage.setItem('_auth.token', token)
    } else {
      throw new Error('Invalid username or password')
    }
    // this.fetchUser()
  }

  @Action({ rawError: true })
  async loginByToken(token: string) {
    this.setToken(token)
    // localStorage.setItem('_auth.token', token)
  }

  @Action({ rawError: true })
  logout() {
    this.setToken('')
    // localStorage.removeItem('_auth.token')
  }
}
