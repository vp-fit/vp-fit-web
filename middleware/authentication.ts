import { Middleware } from '@nuxt/types'
import { getModule } from "vuex-module-decorators";
import authModule from "~/store/auth";

const myMiddleware: Middleware = ({ store, redirect, route }) => {
    const authStore = getModule(authModule, store);
    // const token = localStorage.getItem('_auth.token')
    // if (!process.server) {
    //     if (token) {
    //         const tk = token.split(' ')[1]
    //         if (tk !== undefined && tk !== null && tk != '' && tk !== "undefined") {
    //             authStore.loginByToken(token)
    //         } else {
    //             redirect('/login')
    //         }
    //     } else {
    //         redirect('/login')
    //     }
    // }
}

export default myMiddleware
