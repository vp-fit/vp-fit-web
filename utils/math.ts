/**
 * Converts a number into to decimal places.
 *
 * @param {number} number - The number
 */
export function toTwoDecimalPlaces(number: number): number {
    return Math.round((number + Number.EPSILON) * 100) / 100;
}

export function zeroPaddingNumber(str: string, max: number = 2): string {
    str = str.toString();
    return str.length < max ? zeroPaddingNumber("0" + str, max) : str;
}

export function formatNumberWithDecimal(number: number) {
    return Number(number).toFixed(2).toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ",");
}

export function formatNumber(number: number) {
    return number.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ",");
}

export function abbreviateNumber(n: number) {
    if (n < 1e3 && n > -1e3) return n;
    if ((n >= 1e3 && n < 1e6) || (n <= -1e3 && n > -1e6)) return +(n / 1e3).toFixed(1) + "K";
    if ((n >= 1e6 && n < 1e9) || (n <= -1e6 && n > -1e9)) return +(n / 1e6).toFixed(1) + "M";
    if ((n >= 1e9 && n < 1e12) || (n <= -1e9 && n > -1e12)) return +(n / 1e9).toFixed(1) + "B";
    if (n >= 1e12 || n <= -1e12) return +(n / 1e12).toFixed(1) + "T";
}
