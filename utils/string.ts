export function objToQueryParams(payload: any) {
  var queryString = Object.keys(payload).filter(item => payload[item] !== '' && payload[item] !== undefined && payload[item] !== null).map(key => key + '=' + payload[key]).join('&');
  return !!queryString ? `?${queryString}` : '';
}