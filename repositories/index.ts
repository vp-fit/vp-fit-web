import { NuxtAxiosInstance } from '@nuxtjs/axios'
import AuthRepository from '~/repositories/AuthRepository'
import DashboardRepository from '~/repositories/DashboardRepository'
import MemberRepository from './MemberRepository'
import CustomerRepository from './CustomerRepository'
import TrainerRepository from './TrainerRepository'
import ProgramRepository from './ProgramRepository'
import MemberProgramRepository from './MemberProgramRepository'
import TrainerProgramRepository from './TrainerProgramRepository'
import SaleRepository from './SaleRepository'
import MemberProgramSaleRepository from './MemberProgramSaleRepository'
import TrainerProgramSaleRepository from './TrainerProgramSaleRepository'
import MemberProgramCheckinRepository from './MemberProgramCheckinRepository'
import TrainerProgramCheckinRepository from './TrainerProgramCheckinRepository'

export default ($axios: NuxtAxiosInstance) => ({
    auth: AuthRepository($axios),
    dashboard: DashboardRepository($axios),
    customer: CustomerRepository($axios),
    member: MemberRepository($axios),
    member_program: MemberProgramRepository($axios),
    member_program_sale: MemberProgramSaleRepository($axios),
    member_program_checkin: MemberProgramCheckinRepository($axios),
    trainer: TrainerRepository($axios),
    trainer_program: TrainerProgramRepository($axios),
    trainer_program_checkin: TrainerProgramCheckinRepository($axios),
    trainer_program_sale: TrainerProgramSaleRepository($axios),
    program: ProgramRepository($axios),
    sale: SaleRepository($axios),
})
