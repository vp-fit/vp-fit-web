import { NuxtAxiosInstance } from '@nuxtjs/axios'
import { objToQueryParams } from "~/utils/string";

export interface ProgramRepository {
  create: (payload?: any) => Promise<any>,
  createSell: (payload?: any) => Promise<any>,
  getSell: (member_id?: string) => Promise<any>,
  getSellItem: (program_sell_id?: string) => Promise<any>,
  getSellItemCheckin: (program_sell_id: string, year: number, month: number, day: number) => Promise<any>,
  getCheckinByprogramsell: (sid: string)  => Promise<any>,
  checkin: (mid: string, current_program: string, year: number, month: number, day: number, hour: number, minute: number) => Promise<any>,
  get: (payload?: IRequestData_Program_Get) => Promise<any>,
  update: (payload?: IRequestData_Program_Update) => Promise<any>
}

export default ($axios: NuxtAxiosInstance): ProgramRepository => ({
  async create(payload?: any): Promise<any> {
    let response = await $axios.post(`/program`, payload)
    return response
  },
  async createSell(payload?: any): Promise<any> {
    let response = await $axios.post(`/program/sell`, payload)
    return response
  },
  async getSell(member_id?: string): Promise<any> {
    let response = await $axios.get(`/program/sell/${member_id}`)
    return response
  },
  async getSellItem(program_sell_id?: string): Promise<any> {
    let response = await $axios.get(`/program/sellItem/${program_sell_id}`)
    return response
  },
  async getSellItemCheckin (program_sell_id: string, year: number, month: number, day: number): Promise<any> {
    let response = await $axios.get(`/program/sell/checkinItem/${objToQueryParams({
      program_sell_id,
      year,
      month,
      day
    })}`)
    return response
  },
  async getCheckinByprogramsell (sid: string): Promise<any> {
    let response = await $axios.get(`/program/checkin/byprogramsell/${objToQueryParams({
      sid
    })}`)
    return response
  },
  async checkin(program_sell_id: string, checkin_date: string, year: number, month: number, day: number, hour: number, minute: number) {
    let response = await $axios.post(`/program/sell/checkin`, {
      program_sell_id,
      checkin_date,
      year,
      month,
      day,
      hour,
      minute
    })
    return response
  },
  async get(payload?: IRequestData_Program_Get): Promise<any> {
    let response = await $axios.get(`/program${objToQueryParams(payload)}`)
    return response
  },
  async update(payload?: IRequestData_Program_Update): Promise<any> {
    let response = await $axios.put(`/program`, payload)
    return response
  },
})
