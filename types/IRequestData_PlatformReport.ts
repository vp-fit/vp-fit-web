interface IRequestData_PlatformReport {
  startdate: string | Date,
  enddate: string | Date,
  platformid: string,
  gameids?: string[]
  gameApiId?: string,
  currencies?: string[],
}