interface IRequestData_PlatformGameReport {
  from: number,
  size: number,
  platformid: string,
  games: any[],
  startdate: string | Date,
  enddate: string | Date,
  orderBy?: string,
  currencies?: string[],
}