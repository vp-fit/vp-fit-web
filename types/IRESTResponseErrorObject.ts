interface IRESTResponseErrorObject {
  code: string,
  message: string,
  data?: IObject,
  rid?: string,
}