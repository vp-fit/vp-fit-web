import { NuxtAxiosInstance } from '@nuxtjs/axios'
import { objToQueryParams } from "~/utils/string";

export interface MemberRepository {
  register: (payload?: any) => Promise<any>,
  getMembers: (payload?: IRequestData_Member_GetMembers) => Promise<any>,
  getMember: (mid?: string) => Promise<any>,
  checkin: (mid: string, current_program: string, checkin_date: string, year: number, month: number, day: number, hour: number, minute: number) => Promise<any>,
  getCheckin: (mid: string, year: number, month: number, day: number) => Promise<any>,
  getCheckinBymember: (mid: string) => Promise<any>,
  update: (payload?: IRequestData_Member_Update) => Promise<any>
}

export default ($axios: NuxtAxiosInstance): MemberRepository => ({
  async register(payload?: any): Promise<any> {
    let response = await $axios.post(`/customer`, payload)
    return response
  },
  async getMember(mid?: string): Promise<any> {
    let response = await $axios.get(`/member/${mid}`)
    return response
  },
  async getMembers(payload?: IRequestData_Member_GetMembers): Promise<any> {
    let response = await $axios.get(`/member${objToQueryParams(payload)}`)
    return response
  },
  async checkin(mid: string, current_program: string, checkin_date: string, year: number, month: number, day: number, hour: number, minute: number): Promise<any> {
    let response = await $axios.post(`/member/checkin`, {
      mid,
      current_program,
      checkin_date,
      year,
      month,
      day,
      hour,
      minute
    })
    return response
  },
  async getCheckin (mid: string, year: number, month: number, day: number): Promise<any> {
    let response = await $axios.get(`/member/checkin${objToQueryParams({
      mid,
      year,
      month,
      day
    })}`)
    return response
  },
  async getCheckinBymember(mid: string): Promise<any> {
    let response = await $axios.get(`/member/checkin/bymember${objToQueryParams({
      mid,
    })}`)
    return response
  },
  async update(payload?: IRequestData_Member_Update): Promise<any> {
    let response = await $axios.put(`/member/${payload?._id}`, payload)
    return response
  },
})
