export function validatePhoneNumber(phoneNumber = '') {
  var phoneno = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
  if(phoneNumber.match(phoneno)) {
    return true;
  }
  else {
    return false;
  }
}

export function validateEmail(email = '') {
  var emailRegex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
  if(email.match(emailRegex)) {
    return true;
  }
  else {
    return false;
  }
}