FROM node:12.0.0 as build-stage

ENV APP_HOME=/usr/src/app
ENV API_BASE_URL=https://api.vpfit.kopepan.dev
ENV WEB_TITLE='VP Fit'

RUN mkdir -p $APP_HOME

COPY . $APP_HOME

RUN cd $APP_HOME && npm install

WORKDIR $APP_HOME

RUN npm run generate

# Production Stage
FROM nginx:stable-alpine as production-stage

ENV APP_HOME=/usr/src/app

COPY ./nginx.conf /etc/nginx/conf.d/default.conf

COPY --from=build-stage $APP_HOME/dist /usr/share/nginx/html

RUN chown nginx:nginx /usr/share/nginx/html

EXPOSE 80

CMD ["nginx", "-g", "daemon off;"]

# docker buildx build --platform=linux/amd64 -t 441469793804.dkr.ecr.ap-southeast-1.amazonaws.com/dashboard:1.7.0 .