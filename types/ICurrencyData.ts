interface ICurrencyData {
    id: string;
    _id: string,
    name: string,
    code: string,
};