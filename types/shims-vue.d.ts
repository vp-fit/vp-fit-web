import Vue from "vue";
import { AuthRepository } from "~/repositories/AuthRepository";
import { DashboardRepository } from "~/repositories/DashboardRepository";
import { CustomerRepository } from "~/repositories/CustomerRepository";
import { MemberRepository } from "~/repositories/MemberRepository";
import { MemberProgramRepository } from "~/repositories/MemberProgramRepository";
import { TrainerRepository } from "~/repositories/TrainerRepository";
import { ProgramRepository } from "./../repositories/ProgramRepository";
import { SaleRepository } from "./../repositories/SaleRepository";
import { MemberProgramSaleRepository } from "~/repositories/MemberProgramSaleRepository";
import { TrainerProgramSaleRepository } from "~/repositories/TrainerProgramSaleRepository";
import { MemberProgramCheckinRepository } from "~/repositories/MemberProgramCheckinRepository";
import { TrainerProgramCheckinRepository } from "~/repositories/TrainerProgramCheckinRepository";

declare module "vue/types/vue" {
  interface Vue {
    $repositories: {
      [x: string]: any;
      auth: AuthRepository;
      dashboard: DashboardRepository;
      customer: CustomerRepository;
      member_program: MemberProgramRepository;
      member_program_sale: MemberProgramSaleRepository;
      member_program_checkin: MemberProgramCheckinRepository;
      member: MemberRepository;
      trainer: TrainerRepository;
      program: ProgramRepository;
      trainer_program_sale: TrainerProgramSaleRepository;
      trainer_program_checkin: TrainerProgramCheckinRepository;
      sale: SaleRepository;
    };
  }
}
