interface IPlatformData {
  id: string;
  _id: string,
  name: string,
  wallet_type: string,
  url_namespace: string,
  balance_multiplier: { [key: string]: number },
  game_list: string[],
  billing_percentage: number,
  configs: any,
  is_demo: boolean,
  signature_key: string,
};