import { getModule } from "vuex-module-decorators";

import appModule from "~/store/app";

/**
 * Shows a success dialog.
 *
 * @param {String} message The success message.
 */
export function showSuccessDialog(message: string) {
    // @ts-ignore
    const nuxt: any = $nuxt

    nuxt.$bvModal.msgBoxOk(message, { title: "Success" })
        .catch((error: string) => {
            console.log(error);
        });
}

/**
 * Shows an error dialog.
 *
 * @param {String} message The error message.
 */
export function showErrorDialog(message: string | any) {
    // @ts-ignore
    const nuxt: any = $nuxt

    const msg = typeof message === 'string' ? message : message.message;
    nuxt.$bvModal.msgBoxOk(msg, { title: "Error" })
        .catch((error: any) => {
            console.error(error);
        });
}

/**
 * Shows an error dialog.
 *
 * @param {String} message The error message.
 */
export function showDialog(message: string | any, title: string = '', callback = ()=>{}) {
    // @ts-ignore
    const nuxt: any = $nuxt

    const appStoreModule = getModule(appModule, nuxt.$store);
    const msg = typeof message === 'string' ? message : message.message;
    appStoreModule.alertCallback(callback);
    appStoreModule.alert(msg);
}

/**
 * Shows a confirmation dialog.
 *
 * @param {String} message The message.
 */
export function showConfirmDialog(message: string, okText: string, cancelText: string, onConfirm: Function) {
    // @ts-ignore
    const nuxt: any = $nuxt
    nuxt.$bvModal.msgBoxConfirm(message, {
        title: "Confirm",
        size: "sm",
        okTitle: okText,
        okVariant: "secondary",
        cancelTitle: cancelText,
        cancelVariant: "light",
        footerClass: "p-2",
        hideHeaderClose: false,
        centered: true,
    })
        .then(onConfirm)
        .catch((err: any) => {
            console.log(err);
        });
}