import { NuxtAxiosInstance } from "@nuxtjs/axios";
import { objToQueryParams } from "~/utils/string";
const apipath = "/member_program";

export interface MemberProgramRepository {
  create: (payload?: any) => Promise<any>;
  list: (payload?: any) => Promise<any>;
  get: (id?: string) => Promise<any>;
  update: (payload?: any) => Promise<any>;
  delete: (id?: string) => Promise<any>;
}

export default ($axios: NuxtAxiosInstance): MemberProgramRepository => ({
  async create(payload?: any): Promise<any> {
    let response = await $axios.post(apipath, payload);
    return response;
  },
  async get(id?: string): Promise<any> {
    let response = await $axios.get(`${apipath}/${id}`);
    return response;
  },
  async list(payload?: any): Promise<any> {
    let response = await $axios.get(`${apipath}/${objToQueryParams(payload)}`);
    return response;
  },
  async update(payload?: any): Promise<any> {
    let response = await $axios.put(`${apipath}/${payload?._id}`, payload);
    return response;
  },
  async delete(id?: string): Promise<any> {
    let response = await $axios.delete(`${apipath}/${id}`);
    return response;
  },
});
