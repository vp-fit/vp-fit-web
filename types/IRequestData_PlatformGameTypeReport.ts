interface IRequestData_PlatformGameTypeReport {
  startdate: string | Date,
  enddate: string | Date,
  platformid: string,
  type: string,
  gameids?: string[]
  gameApiId?: string,
  currencies?: string[],
}