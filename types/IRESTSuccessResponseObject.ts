interface IRESTErrorResponseObject {
  status: number,
  error: IRESTResponseErrorObject,
}