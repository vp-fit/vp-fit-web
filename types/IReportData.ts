interface IReportData {
  activeUser: number,
  betCount: number,
  biling: number,
  hitRate?: number,
  loseCount: number,
  id?: string,
  name?: string,
  rtp?: number,
  turnover: number,
  turnoverPercen: number,
  winAmount: number,
  winCount: number,
  winLose: number,
  winLosePercen: number,
  gameApiBiling: {name: string, biling: number}[]
}